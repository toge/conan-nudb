import os
from conans import ConanFile, tools

required_conan_version = ">=1.33.0"

class NudbConan(ConanFile):
    name = "nudb"
    license = "BSL-1.0"
    url = "https://github.com/conan-io/conan-center-index"
    homepage = "https://github.com/CPPAlliance/NuDB"
    description = "NuDB: A fast key/value insert-only database for SSD drives in C++11"
    topics = ("header-only", "KVS", )
    no_copy_source = True
    requires = ["boost/1.77.0", ]

    @property
    def _source_subfolder(self):
        return "source_subfolder"

    def source(self):
        tools.get(**self.conan_data["sources"][self.version],
            destination=self._source_subfolder, strip_root=True)

    def package(self):
        self.copy("*.hpp", dst="include", src=os.path.join(self._source_subfolder, "include"))
        self.copy("*.ipp", dst="include", src=os.path.join(self._source_subfolder, "include"))

    def package_id(self):
        self.info.header_only()
