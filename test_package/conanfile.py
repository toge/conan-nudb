import os

from conans import ConanFile, CMake, tools


class NudbTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake", "cmake_find_package_multi"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def test(self):
        if not tools.cross_building(self):
            bin_cpp_path = os.path.join("bin", "example")
            self.run(bin_cpp_path, run_environment=True)